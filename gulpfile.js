var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles', function(){
  return gulp.src('src/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('dist'))
});